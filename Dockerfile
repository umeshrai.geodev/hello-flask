FROM alpine:latest

WORKDIR /app 

RUN apk update 
RUN apk add python3 py3-pip --no-cache

RUN pip install flask

COPY app.py /app/

ENTRYPOINT FLASK_APP=/app/app.py flask run --host=0.0.0.0 --port=8080